#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import warnings

from sqlite3 import connect, Error as DatabaseError



class CDatabase(object):

    def __init__(self, database):
        warnings.filterwarnings("ignore", category=Warning)
        if not os.path.isfile('comments.db'):
            open_db = open('SQLite/script.sql', 'r')
            sql = ''.join(open_db.readlines())
            connect(database).cursor().executescript(sql)
            connect(database).commit()
        self._db = connect(database)


    def select(self, statement, isFirst=False):
        cursor = self._db.cursor()
        cursor.execute(statement)
        result = [dict((cursor.description[index][0], field) for index, field in enumerate(record))
                  for record in cursor.fetchall()]
        return result if not isFirst else result[0] if result else {}

    def execute(self, statement):
        cursor = self._db.cursor()
        return cursor.execute(statement)

    def commit(self):
        self._db.commit()

    def rollback(self):
        self._db.rollback()

    def close(self):
        self._db.close()
